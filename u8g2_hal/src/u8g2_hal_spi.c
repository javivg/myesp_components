/**
 * The MIT License (see LICENSE.txt)
 *
 * @file u8g2_hal_spi.c
 *
 * @brief u8g2 HAL for ESP32 SPI interface source code.
 *
 * u8g2 library created by olikraus:
 * https://github.com/olikraus/u8g2
 *
 * @name Javier Vargas Garcia
 * @date 10/01/2022
 */

#include <stdio.h>
#include <string.h>

#include "u8g2_hal.h"

/* Forward callback declarations */
static uint8_t u8g2_hal_spi_byte_cb(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr);
extern uint8_t u8g2_hal_gpio_and_delay_cb(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr);

esp_err_t u8g2_hal_spi_init(u8g2_t* u8g2, const u8g2_hal_config_t* config, u8g2_screen_setup_f screen_f, const u8g2_cb_t *rotation) {
	esp_err_t err = ESP_FAIL;

	u8g2_hal handler = NULL;

    /* Check inputs */
    if (u8g2 == NULL || config == NULL || screen_f == NULL) goto init_end;

    /* Create a new handler */
    handler = malloc(sizeof(u8g2_hal_t));
    if (handler == NULL) goto init_end;
	memset(handler, 0, sizeof(u8g2_hal_t));

	/* Copy configuration */
	memcpy(&handler->config, config, sizeof(u8g2_hal_config_t));

	/* Initialize SPI bus */
	if (handler->config.gpio_sclk != -1 && handler->config.gpio_sclk != -1) {
		spi_bus_config_t bus_config = {0};
		bus_config.sclk_io_num   = handler->config.gpio_sclk;
		bus_config.mosi_io_num   = handler->config.gpio_mosi;
		bus_config.miso_io_num   = -1;
		bus_config.quadwp_io_num = -1;
		bus_config.quadhd_io_num = -1;
		err = spi_bus_initialize(handler->config.host_id, &bus_config, SPI_DMA_CH_AUTO);
		if (err) goto init_end;
	}

	/* Add device configuration */
	spi_device_interface_config_t dev_cfg = {0};
	dev_cfg.clock_speed_hz = handler->config.clock_speed_hz;
	dev_cfg.spics_io_num = handler->config.gpio_cs;
	dev_cfg.queue_size = 1;
	err = spi_bus_add_device(handler->config.host_id, &dev_cfg, &handler->spi);
	if (err) goto init_end;

	/* Save handler in user pointer from u8x8 structure */
	u8x8_SetUserPtr(u8g2_GetU8x8(u8g2), handler);

	/* Add callback functions */
	screen_f(u8g2, rotation, u8g2_hal_spi_byte_cb, u8g2_hal_gpio_and_delay_cb);

init_end:

	/* Free handler if there is an error */
	if (err && handler != NULL) {
		free(handler);
		handler = NULL;
	}

	return err;
}

static uint8_t u8g2_hal_spi_byte_cb(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr) {
	esp_err_t err = ESP_OK;
	spi_transaction_t trans_desc;

	/* Get HAL handler */
	u8g2_hal handler = u8x8_GetUserPtr(u8x8);

	switch(msg) {

		/* Set DC pin */
		case U8X8_MSG_BYTE_SET_DC:
			err = gpio_set_level(handler->config.gpio_dc, arg_int);
			break;

		/* Send SPI transaction */
		case U8X8_MSG_BYTE_SEND:
			memset(&trans_desc, 0, sizeof(trans_desc));
			trans_desc.length = arg_int * 8;
			trans_desc.tx_buffer = arg_ptr;
			err = spi_device_transmit(handler->spi, &trans_desc);
			break;

		/* Some events are not handled */
		default:
			break;
	}

	/* Error callback */
	if (err && handler->error_cb != NULL) handler->error_cb(err);

	return 0;
}

