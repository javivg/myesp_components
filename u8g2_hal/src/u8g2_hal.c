/**
 * The MIT License (see LICENSE.txt)
 *
 * @file u8g2_hal.c
 *
 * @brief u8g2 HAL for ESP32 source code.
 *
 * u8g2 library created by olikraus:
 * https://github.com/olikraus/u8g2
 *
 * @name Javier Vargas Garcia
 * @date 10/01/2022
 */

#include <stdio.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp32/rom/ets_sys.h"

#include "u8g2_hal.h"

esp_err_t u8g2_hal_error_callback(u8g2_t* u8g2, u8g2_hal_error_cb error_cb) {
	esp_err_t err = ESP_FAIL;
	/* Get HAL handler */
	u8g2_hal handler = u8x8_GetUserPtr(u8g2_GetU8x8(u8g2));
	if (handler != NULL && error_cb != NULL) {
		/* Set error callback function */
		handler->error_cb = error_cb;
		err = ESP_OK;
	}
	return err;
}

uint8_t u8g2_hal_gpio_and_delay_cb(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr) {
	esp_err_t err = ESP_OK;

	/* Get HAL handler */
	u8g2_hal handler = u8x8_GetUserPtr(u8x8);

	switch(msg) {

		/* Initialize reset and DC outputs */
		case U8X8_MSG_GPIO_AND_DELAY_INIT:
			err = gpio_reset_pin(handler->config.gpio_reset);
			err |= gpio_set_direction(handler->config.gpio_reset, GPIO_MODE_OUTPUT);
			err |= gpio_reset_pin(handler->config.gpio_dc);
			err |= gpio_set_direction(handler->config.gpio_dc, GPIO_MODE_OUTPUT);
			break;

		/* Set the GPIO reset pin to the value passed in through arg_int */
		case U8X8_MSG_GPIO_RESET:
			err = gpio_set_level(handler->config.gpio_reset, arg_int);
			break;

		/* Delay for the number of milliseconds passed in through arg_int */
		case U8X8_MSG_DELAY_MILLI:
			if (arg_int >= portTICK_PERIOD_MS) vTaskDelay(arg_int/portTICK_PERIOD_MS);
			else ets_delay_us(1000*arg_int);
			break;

			/* Some events are not handled */
		default:
			break;
	}

	/* Error callback */
	if (err && handler->error_cb != NULL) handler->error_cb(err);

	return 0;
}



