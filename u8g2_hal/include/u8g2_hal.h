/**
 * The MIT License (see LICENSE.txt)
 *
 * @file u8g2_hal.h
 *
 * @brief Hardware Abstraction Layer (HAL) to use u8g2 library with ESP32.
 *
 * u8g2 library created by olikraus:
 * https://github.com/olikraus/u8g2
 *
 * @name Javier Vargas Garcia
 * @date 10/01/2022
 */

#ifndef COMPONENTS_MYESP_COMPONENTS_U8G2_HAL_U8G2_HAL_H_
#define COMPONENTS_MYESP_COMPONENTS_U8G2_HAL_U8G2_HAL_H_

#include "driver/gpio.h"
#include "hal/spi_types.h"
#include "driver/spi_master.h"
#include "esp_err.h"

/* NOTE:
 * u8g2 library must be compiled with the flag U8X8_WITH_USER_PTR
 * This component add this flag in CMakeList.txt
 * idf_build_set_property(COMPILE_OPTIONS "-DU8X8_WITH_USER_PTR" APPEND)
 */
#include "u8g2.h"

/* User configuration structure */
typedef struct {
	gpio_num_t gpio_mosi; 		//SPI MOSI pin (-1 to not initialize a new SPI bus)
	gpio_num_t gpio_sclk; 		//SPI SCLK pin (-1 to not initialize a new SPI bus)
	spi_host_device_t host_id; 	//SPI host
	gpio_num_t gpio_cs; 		//Chip Select
	gpio_num_t gpio_dc; 		//Data Control
	gpio_num_t gpio_reset; 		//Reset
	int clock_speed_hz;			//Clock speed in Hz
} u8g2_hal_config_t;

/* Error callback */
typedef void (*u8g2_hal_error_cb)(esp_err_t err);

/* Handler structure */
typedef struct {
	u8g2_hal_config_t config;
	spi_device_handle_t spi;
	u8g2_hal_error_cb error_cb;
} u8g2_hal_t;

typedef u8g2_hal_t* u8g2_hal;

/* Setup function prototype from u8g2_d_steup.c */
typedef void (*u8g2_screen_setup_f)(u8g2_t *u8g2, const u8g2_cb_t *rotation, u8x8_msg_cb byte_cb, u8x8_msg_cb gpio_and_delay_cb);

esp_err_t u8g2_hal_spi_init(u8g2_t* u8g2, const u8g2_hal_config_t* config, u8g2_screen_setup_f screen_f, const u8g2_cb_t *rotation);
esp_err_t u8g2_hal_error_callback(u8g2_t* u8g2, u8g2_hal_error_cb error_cb);

#endif /* COMPONENTS_MYESP_COMPONENTS_U8G2_HAL_U8G2_HAL_H_ */
