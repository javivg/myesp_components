/**
 * The MIT License (see LICENSE.txt)
 *
 * @file button.h
 *
 * @brief This module provides a set of functions to handle a set of button events.
 *
 * @name Javier Vargas Garcia
 * @date 30/12/2021
 */

#ifndef COMPONENTS_MYESP_COMPONENTS_BUTTON_BUTTON_H_
#define COMPONENTS_MYESP_COMPONENTS_BUTTON_BUTTON_H_

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "driver/gpio.h"
#include "esp_err.h"

/* Forward declarations */
typedef struct button_handler_t button_handler_t;
typedef struct button_task_handler_t button_task_handler_t;

/* Handler pointer */
typedef button_handler_t* button_handler;
typedef button_task_handler_t* button_task_handler;

/* Events */
typedef enum {
	button_evt_interrupt,
	button_evt_released,
	button_evt_pushed,
	button_evt_push_short,
	button_evt_push_long,
} button_evt_t;

/* Callback function */
typedef void (*button_event_cb)(button_handler handler, button_evt_t button_evt_t);

/* Button configuration structure */
typedef struct {
	gpio_num_t gpio;
	gpio_pull_mode_t pull;
	int push_lvl;
	uint32_t long_push_ms;
} button_config_t;

/* Button handler */
struct button_handler_t {
	button_config_t config;
	volatile button_event_cb event_cb;
	TickType_t tick;
	bool push_status;
	bool evt_status;
	volatile button_task_handler task_handler;
	button_event_cb task_event_cb;
	button_evt_t task_poll_evt;
};

/* Botton task configuration structure */
typedef struct {
	char* name;
	int priority;
	BaseType_t cpu;
	uint32_t poll_period_ms;
} button_task_config_t;

/* Handler button task structure */
struct button_task_handler_t {
	button_task_config_t config;
	TaskHandle_t task;
	SemaphoreHandle_t semaphore;
	button_handler* volatile buttons;
	volatile size_t buttons_count;
};

esp_err_t button_init(button_handler* handler, const button_config_t* config, button_event_cb event_cb, bool install_gpio_isr);
button_evt_t button_read(button_handler handler);
esp_err_t button_evt_start(button_handler handler);
esp_err_t button_evt_stop(button_handler handler);

esp_err_t button_task_init(button_task_handler* task_handler, const button_task_config_t* task_config, button_handler* buttons, size_t buttons_size);
esp_err_t button_task_evt_start(button_task_handler task_handler);
esp_err_t button_task_evt_stop(button_task_handler task_handler);

#endif /* COMPONENTS_MYESP_COMPONENTS_BUTTON_BUTTON_H_ */
