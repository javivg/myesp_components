/**
 * The MIT License (see LICENSE.txt)
 *
 * @file button.c
 *
 * @brief Button handler source code.
 *
 * @name Javier Vargas Garcia
 * @date 30/12/2021
 */

#include <string.h>
#include "sys/lock.h"

#include "button.h"

// A flag to identify if gpio isr service has been installed.
static bool is_gpio_isr_service_installed = false;
// A lock to avoid gpio isr service being installed twice in multiple threads.
static _lock_t isr_service_install_lock;
// A lock to handle button task concurrent accesses
extern _lock_t button_task_lock;

static void gpio_isr_handler(void *arg) {
	button_handler handler = arg;
	/* Execute user event callback just to notify an interrupt */
	handler->event_cb(handler, button_evt_interrupt);
}

esp_err_t button_init(button_handler* handler, const button_config_t* config, button_event_cb event_cb, bool install_gpio_isr) {
	esp_err_t err = ESP_FAIL;
	button_handler new_handler = NULL;

	/* Check input values */
	if (handler == NULL || config == NULL) goto init_end;

	/* Create handler in heap */
	new_handler = malloc(sizeof(button_handler_t));
	if (new_handler == NULL) goto init_end;
	memset(new_handler, 0, sizeof(button_handler_t));

	/* Copy configuration */
	memcpy(&new_handler->config, config, sizeof(button_config_t));
	new_handler->event_cb = event_cb;

	/* Set gpio configuration */
	if (new_handler->config.push_lvl != 0) new_handler->config.push_lvl = 1;
	err = gpio_reset_pin(new_handler->config.gpio);
	err |= gpio_set_direction(new_handler->config.gpio, GPIO_MODE_INPUT);
	err |= gpio_set_pull_mode(new_handler->config.gpio, new_handler->config.pull);
	err |= gpio_set_intr_type(new_handler->config.gpio, new_handler->config.push_lvl == 0 ? GPIO_INTR_NEGEDGE : GPIO_INTR_POSEDGE);
	err |= gpio_intr_disable(new_handler->config.gpio);
	if (err) goto init_end;

	/* Register interrupt handler in a thread-safe way only once */
	_lock_acquire(&isr_service_install_lock);
	if (!is_gpio_isr_service_installed && install_gpio_isr) {
		err = gpio_install_isr_service(0);
		if (err) goto init_end;
		is_gpio_isr_service_installed = true;
	}
	_lock_release(&isr_service_install_lock);

	/* Add interrupt handler */
	if (event_cb != NULL) {
		err = gpio_isr_handler_add(new_handler->config.gpio, gpio_isr_handler, new_handler);
		if (err) goto init_end;
	}

init_end:
	/* Free handler if there is an error */
	if (err && new_handler != NULL) {
		free(new_handler);
		new_handler = NULL;
	}

	*handler = new_handler;
	return err;
}

button_evt_t button_read(button_handler handler) {

	/* Read gpio level */
	int lvl = gpio_get_level(handler->config.gpio);
	button_evt_t evt = lvl == handler->config.push_lvl ? button_evt_pushed : button_evt_released;

	/* Read tick */
	TickType_t tick = xTaskGetTickCount();

	/* Pushed */
	if (handler->push_status == 0 && lvl == handler->config.push_lvl) {
		handler->tick = tick;
		handler->push_status = 1;
	}

	/* Released */
	else if (handler->push_status == 1 && lvl == !handler->config.push_lvl) {
		handler->push_status = 0;
		/* Long push */
		if ((tick - handler->tick) > pdMS_TO_TICKS(handler->config.long_push_ms)) evt = button_evt_push_long;
		/* Short push */
		else evt = button_evt_push_short;
	}

	return evt;
}

esp_err_t button_evt_start(button_handler handler) {
	esp_err_t err;
	_lock_acquire(&button_task_lock);
	handler->evt_status = 1;
	err = gpio_intr_enable(handler->config.gpio);
	_lock_release(&button_task_lock);
	return err;
}

esp_err_t button_evt_stop(button_handler handler) {
	esp_err_t err;
	_lock_acquire(&button_task_lock);
	handler->evt_status = 0;
	err = gpio_intr_disable(handler->config.gpio);
	_lock_release(&button_task_lock);
	return err;
}



