/**
 * The MIT License (see LICENSE.txt)
 *
 * @file button_task.c
 *
 * @brief Button task source code.
 *
 * @name Javier Vargas Garcia
 * @date 30/12/2021
 */

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "button.h"

#define TASK_STACK_SIZE 2048

_lock_t button_task_lock;

static void button_callback(button_handler handler, button_evt_t evt) {
	BaseType_t higher_priority_task = pdFALSE;

	/* Disable all interrupts */
	for (int idx = 0; idx < handler->task_handler->buttons_count; idx++) {
		gpio_intr_disable(handler->task_handler->buttons[idx]->config.gpio);
	}

	/* Signal this interrupt to handle it from task */
	xSemaphoreGiveFromISR(handler->task_handler->semaphore, &higher_priority_task);

	/* Run scheduler if there is a higher priority task released */
	if (higher_priority_task) portYIELD_FROM_ISR();
}

static void button_task(void* arg) {
	button_task_handler task_handler = arg;

	for (;;) {

		/* Wait semaphore to be released from gpio ISR */
		if (xSemaphoreTake(task_handler->semaphore, portMAX_DELAY) == pdTRUE) {

			/* Polling */
			while (1) {

				button_evt_t global_evt = (button_evt_released+1);

				/* Loop through all buttons */
				for (int idx = 0; idx < task_handler->buttons_count; idx++) {
					button_handler handler = task_handler->buttons[idx];

					/* Read button */
					button_evt_t evt = button_read(handler);
					global_evt |= (evt+1);

					/* Send only new events */
					if (evt != handler->task_poll_evt) {
						handler->task_poll_evt = evt;

						/* Event callback */
						if (handler->task_event_cb) handler->task_event_cb(handler, evt);
					}
				}

				/* Stop polling when all buttons are released */
				if (global_evt == button_evt_released+1) break;

				vTaskDelay(pdMS_TO_TICKS(task_handler->config.poll_period_ms));
			}

			_lock_acquire(&button_task_lock);

			/* Enable interrupts */
			for (int idx = 0; idx < task_handler->buttons_count; idx++) {
				button_handler handler = task_handler->buttons[idx];
				if (handler->evt_status) gpio_intr_enable(handler->config.gpio);
			}

			_lock_release(&button_task_lock);
		}
	}
}

esp_err_t button_task_init(button_task_handler* task_handler, const button_task_config_t* task_config, button_handler* buttons, size_t buttons_size) {
	esp_err_t err = ESP_FAIL;
	button_task_handler new_task_handler = NULL;

	/* Check input values */
	if (task_handler == NULL || task_config == NULL || buttons == NULL || buttons_size % sizeof(button_handler) != 0 || buttons_size == 0) goto init_end;

	/* Create handler in heap */
	new_task_handler = malloc(sizeof(button_task_handler_t));
	if (new_task_handler == NULL) goto init_end;
	memset(new_task_handler, 0, sizeof(button_task_handler_t));

	/* Copy configuration */
	memcpy(&new_task_handler->config, task_config, sizeof(button_task_config_t));
	new_task_handler->buttons_count = buttons_size / sizeof(button_handler);

	/* Add memory for buttons handler array and copy them */
	new_task_handler->buttons = malloc(buttons_size);
	if (new_task_handler->buttons == NULL) goto init_end;
	memcpy(new_task_handler->buttons, buttons, buttons_size);

	/* Create semaphore */
	new_task_handler->semaphore = xSemaphoreCreateBinary();
	if (new_task_handler->semaphore == NULL) goto init_end;

	/* Loop through buttons */
	for (int idx = 0; idx < new_task_handler->buttons_count; idx++) {
		button_handler handler = new_task_handler->buttons[idx];

		/* Assign this task handler to button object */
		if (handler != NULL && handler->task_handler == NULL) handler->task_handler = new_task_handler;
		else { goto init_end; }

		/* Redirect button event callback to button task event callback. Now task handles this callback */
		handler->task_event_cb = handler->event_cb;
		handler->event_cb = button_callback;

		/* Released status */
		handler->task_poll_evt = button_evt_released;
	}

	/* Create task */
	if (xTaskCreatePinnedToCore(&button_task, new_task_handler->config.name,
			TASK_STACK_SIZE, new_task_handler, new_task_handler->config.priority,
			&new_task_handler->task, new_task_handler->config.cpu) == pdTRUE) err = ESP_OK;

init_end:

	/* Free handler if there is an error */
	if (err && new_task_handler != NULL) {
		if (new_task_handler->semaphore != NULL) vSemaphoreDelete(new_task_handler->semaphore);
		if (new_task_handler->buttons != NULL) free(new_task_handler->buttons);
		free(new_task_handler);
		new_task_handler = NULL;
	}

	*task_handler = new_task_handler;
	return err;
}

esp_err_t button_task_evt_start(button_task_handler task_handler) {
	esp_err_t err = ESP_OK;
	_lock_acquire(&button_task_lock);
	/* Enable interrupts */
	for (int idx = 0; idx < task_handler->buttons_count; idx++) {
		button_handler handler = task_handler->buttons[idx];
		handler->evt_status = 1;
		err |= gpio_intr_enable(handler->config.gpio);
	}
	_lock_release(&button_task_lock);
	return err;
}

esp_err_t button_task_evt_stop(button_task_handler task_handler) {
	esp_err_t err = ESP_OK;
	_lock_acquire(&button_task_lock);
	/* Disable interrupts */
	for (int idx = 0; idx < task_handler->buttons_count; idx++) {
		button_handler handler = task_handler->buttons[idx];
		handler->evt_status = 0;
		err |= gpio_intr_disable(handler->config.gpio);
	}
	_lock_release(&button_task_lock);
	return err;
}
