/**
 * The MIT License (see LICENSE.txt)
 *
 * @file tone.c
 *
 * @brief Tone library source code.
 *
 * @name Javier Vargas Garcia
 * @date 06/06/2022
 */

#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"

#include "tone.h"

/* Static configuration */

#define LEDC_SPEED_MODE 		LEDC_HIGH_SPEED_MODE
#define LEDC_DUTY_RESOLUTION 	LEDC_TIMER_8_BIT
#define LEDC_CLK				LEDC_AUTO_CLK
#define TASK_STACK_SIZE			2048

/* Operations */

#define DUTY_ON 				((1<<LEDC_DUTY_RESOLUTION)/4)
#define TONE_TICKS_TO_MS(ms) 	(ms == TONE_MS_BLOCK ? portMAX_DELAY : pdMS_TO_TICKS(ms))

/* Type definitions */

struct tone_handler_t {
	ledc_timer_config_t timer;
	ledc_channel_config_t channel;
	TaskHandle_t task;
	QueueHandle_t queue;
	SemaphoreHandle_t mutex;
	tone_task_error_cb error_cb;
	bool enabled;
};

/* Private functions declaration */

static esp_err_t tone_note_(tone_handler tone, note_t note);
static esp_err_t tone_sound_(tone_handler tone, const tone_sound_t* sound, size_t sound_size);
static void tone_task(void* arg);

/* Functions */

esp_err_t tone_init(tone_handler* tone, ledc_timer_t timer, ledc_channel_t channel, gpio_num_t gpio, const tone_task_config_t* task_config) {
	esp_err_t err = ESP_FAIL;
	tone_handler handler = NULL;

	/* Check input values */
	if (tone == NULL) goto init_end;

	/* Create handler in heap */
	handler = malloc(sizeof(tone_handler_t));
	if (handler == NULL) goto init_end;
	memset(handler, 0, sizeof(tone_handler_t));

	/* Setup LEDC structures */
	handler->timer.speed_mode = LEDC_SPEED_MODE;
	handler->timer.duty_resolution = LEDC_DUTY_RESOLUTION;
	handler->timer.clk_cfg = LEDC_CLK;
	handler->timer.freq_hz = (uint32_t)note_C4;
	handler->timer.timer_num = timer;
	handler->channel.channel = channel;
	handler->channel.speed_mode = LEDC_SPEED_MODE;
	handler->channel.intr_type = LEDC_INTR_DISABLE;
	handler->channel.timer_sel = timer;
	handler->channel.gpio_num = gpio;

	/* Initialize LEDC module */
	err = ledc_timer_config(&handler->timer);
	if (err) goto init_end;
	err = ledc_channel_config(&handler->channel);
	if (err) goto init_end;

	/* Mutex to shared resources */
	handler->mutex = xSemaphoreCreateMutex();
	if (handler->mutex == NULL) goto init_end;

	/* Task mode configuration (optional) */
	if (task_config != NULL) {
		/* Set error callback (optional) */
		handler->error_cb = task_config->error_cb;
		/* Create queue */
		handler->queue = xQueueCreate(task_config->queue_elements, sizeof(tone_sound_list_t));
		if (handler->queue == NULL) { err = ESP_FAIL; goto init_end; }
		/* Create task */
		if (xTaskCreatePinnedToCore(&tone_task, task_config->name, TASK_STACK_SIZE, handler, task_config->priority,
				&handler->task, task_config->cpu) != pdTRUE) err = ESP_FAIL;
	}


init_end:

	/* Free handler if there is an error */
	if (err && handler != NULL) {
		if (handler->mutex != NULL) vSemaphoreDelete(handler->mutex);
		if (handler->task != NULL) vTaskDelete(handler->task);
		free(handler);
		handler = NULL;
	}

	*tone = handler;
	return err;
}

static esp_err_t tone_note_(tone_handler tone, note_t note) {
	esp_err_t err = ESP_OK;
	if (note == note_none) {
		/* Disable signal */
		if (tone->enabled) {
			tone->enabled = 0;
			err = ledc_set_duty(tone->channel.speed_mode, tone->channel.channel, 0UL);
			if (!err) err = ledc_update_duty(tone->channel.speed_mode, tone->channel.channel);
		}
	}
	else {
		/* Setup LEDC timer frequency */
		tone->timer.freq_hz = (uint32_t)note;
		err = ledc_timer_config(&tone->timer);
		/* Enable signal */
		if (!err && !tone->enabled) {
			tone->enabled = 1;
			err = ledc_set_duty(tone->channel.speed_mode, tone->channel.channel, (uint32_t)DUTY_ON);
			if (!err) ledc_update_duty(tone->channel.speed_mode, tone->channel.channel);
		}
	}
	return err;
}

esp_err_t tone_note(tone_handler tone, note_t note, uint32_t block_ms) {
	esp_err_t err;
	BaseType_t ret_;
	/* Wait to take control */
	do { ret_ = xSemaphoreTake(tone->mutex, TONE_TICKS_TO_MS(block_ms));
	} while (ret_ != pdTRUE && block_ms == TONE_MS_BLOCK);
	if (ret_ == pdTRUE) {
		/* Set tone */
		err = tone_note_(tone, note);
		/* Release control */
		xSemaphoreGive(tone->mutex);
	}
	/* Mutex wait timeout */
	else err = ESP_ERR_TIMEOUT;
	return err;
}

static esp_err_t tone_sound_(tone_handler tone, const tone_sound_t* sound, size_t sound_size) {
	esp_err_t err = ESP_OK;
	esp_err_t err_aux;
	uint32_t notes = sound_size / sizeof(tone_sound_t);
	for (uint32_t i = 0; i < notes; i++) {
		/* Set note */
		err = tone_note_(tone, sound[i].tone);
		if (err) break;
		/* Wait time */
		vTaskDelay(pdMS_TO_TICKS(sound[i].ms));
	}
	/* Stop sound */
	err_aux = tone_note_(tone, note_none);
	if (err_aux && !err) err = err_aux;
	return err;
}

esp_err_t tone_sound(tone_handler tone, const tone_sound_list_t* sound_list, uint32_t block_ms) {
	esp_err_t err = ESP_OK;
	BaseType_t ret_;
	/* Play sound in background from task */
	if (tone->task != NULL) {
		/* Wait to send queue */
		do {ret_ = xQueueSend(tone->queue, sound_list, TONE_TICKS_TO_MS(block_ms));
		} while (ret_ != pdTRUE && block_ms == TONE_MS_BLOCK);
		/* Send queue timeout */
		if (ret_ != pdTRUE) err = ESP_ERR_TIMEOUT;
	}
	/* Play sound from caller task (blocking) */
	else {
		/* Wait to take control */
		do { ret_ = xSemaphoreTake(tone->mutex, TONE_TICKS_TO_MS(block_ms));
		} while (ret_ != pdTRUE && block_ms == TONE_MS_BLOCK);
		if (ret_ == pdTRUE) {
			/* Play sound */
			err = tone_sound_(tone, sound_list->sound, sound_list->size);
			/* Release control */
			xSemaphoreGive(tone->mutex);
		}
		/* Mutex wait timeout */
		else err = ESP_ERR_TIMEOUT;
	}
	return err;
}

static void tone_task(void* arg) {
	esp_err_t err;
	BaseType_t ret_;
	tone_handler tone = (tone_handler)arg;
	tone_sound_list_t sound_list;
	for (;;) {
		/* Wait incoming sounds from queue */
		if(xQueueReceive(tone->queue, &sound_list, portMAX_DELAY) == pdPASS) {
			/* Take control */
			do { ret_ = xSemaphoreTake(tone->mutex, portMAX_DELAY);
			} while (ret_ != pdTRUE);
			/* Play sound */
			err = tone_sound_(tone, sound_list.sound, sound_list.size);
			/* Release control */
			xSemaphoreGive(tone->mutex);
			/* Report errors through callback (optional) */
			if (err && tone->error_cb != NULL) tone->error_cb(tone, err);
		}
	}
}



