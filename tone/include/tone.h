/**
 * The MIT License (see LICENSE.txt)
 *
 * @file tone.h
 *
 * @brief This library provides functions to control a buzzer.
 *
 * It uses ESP32 LEDC hardware module to output an square signal with the desired frequency.
 * User is able to define a set of timed sounds to be played from a background task.
 *
 * @name Javier Vargas Garcia
 * @date 06/06/2022
 */

#ifndef COMPONENTS_MYESP_COMPONENTS_TONE_INCLUDE_TONE_H_
#define COMPONENTS_MYESP_COMPONENTS_TONE_INCLUDE_TONE_H_

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_err.h"

#include "note.h"

/**
 * Block time forever (wait time does not expires)
 */
#define TONE_MS_BLOCK 0xFFFFFFFFUL

/**
 * Forward declarations
 */
typedef struct tone_handler_t tone_handler_t;

/**
 * TONE handler
 */
typedef tone_handler_t* tone_handler;

/**
 * Task mode error callback function
 */
typedef void (*tone_task_error_cb)(tone_handler tone, esp_err_t err);

/**
 * Sound element structure.
 */
typedef struct {
	note_t tone; ///< Tone enumeration with corresponding frequency value.
	uint32_t ms; ///< Duration in milliseconds for this tone sound.
} tone_sound_t;

/**
 * Sound list structure.
 */
typedef struct {
	const tone_sound_t* sound; ///< Pointer to an array of tone_sound_t.
	size_t size; ///< Size of sound array in bytes.
} tone_sound_list_t;

/**
 * Task mode configuration structure.
 */
typedef struct {
	char* name; ///< String with the name of task.
	int priority; ///< Priority of the task.
	BaseType_t cpu; ///< CPU where task must run.
	uint32_t queue_elements; ///< Maximum queued elements for sending task sounds.
	tone_task_error_cb error_cb; ///< Error callback to be executed from task when arise an error (NULL to ignore).
} tone_task_config_t;

/**
 * @brief Initialize a TONE handler attached to LEDC timer, channel and GPIO.
 *
 * Timer, channel and GPIO shall not be used by any other component.
 * An additional (and optional) task configuration structure can be used to deploy a FreeRTOS task which takes the control
 * of tone_sound() execution so no blocking API is provided.
 *
 * @param tone[out]			TONE handler initialized and used by all API calls.
 * @param timer[in]			Timer from LEDC hardware to be used.
 * @param channel[in]		Channel from LEDC hardware to be used.
 * @param gpio[in]			Output GPIO attached.
 * @param task_config[in]	Task configuration structure (optional, NULL to ignore). See tone_task_config_t for setup members information.
 *
 * @return 	ESP_OK if success or standard ESP error code otherwise.
 */
esp_err_t tone_init(tone_handler* tone, ledc_timer_t timer, ledc_channel_t channel, gpio_num_t gpio, const tone_task_config_t* task_config);

/**
 * @brief Set output frequency to enumerated note.
 *
 * @param tone[in]		TONE handler initialized by tone_init().
 * @param note[in]		Note to be played from enumeration type. Any additional numeric frequency is valid.
 * @param block_ms[in]	Block time in milliseconds to take LEDC control (TONE_MS_BLOCK to block forever).
 *
 * @return	ESP_OK if success, ESP_ERR_TIMEOUT if block time expires or standard ESP error code otherwise.
 */
esp_err_t tone_note(tone_handler tone, note_t note, uint32_t block_ms);

/**
 * @brief Play a set of collected notes within a sound list.
 *
 * Play a series of timed sounds pointed by sound_list->sound and sized with sound_list->size.
 * User might want to define a tone_sound_list_t array pointing to each tone_sound_t data,
 * so each tone_sound_list_t element is a full play tone.
 *
 * When task mode is enabled (task configuration initialized), this function will send sounds to task
 * to be played in background and block_ms time will be applied to sending queue wait.
 *
 * When task mode is disabled, sounds will be played from caller thus function is blocking while sound is being played.
 * In this case block_ms time will be applied to get access to LEDC hardware which might be shared when the same handler
 * is used in a multi-task context.
 *
 * @param tone[in]			TONE handler initialized by tone_init().
 * @param sound_list[in]	Pointer to structure with constant sound array and size of sound array.
 * @param block_ms[in]		Block time in milliseconds to send data to tone task (when initialized) or take LEDC control (TONE_MS_BLOCK to block forever).
 *
 * @note Sound array from sound_list->sound MUST be STATIC CONSTANTS whenever task mode is enabled.
 *
 * @return ESP_OK if success, ESP_ERR_TIMEOUT if block time expires or standard ESP error code otherwise.
 */
esp_err_t tone_sound(tone_handler tone, const tone_sound_list_t* sound_list, uint32_t block_ms);

#endif /* COMPONENTS_MYESP_COMPONENTS_TONE_INCLUDE_TONE_H_ */
