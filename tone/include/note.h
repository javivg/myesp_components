/**
 * The MIT License (see LICENSE.txt)
 *
 * @file note.h
 *
 * @brief Enumeration with notes frequencies.
 *
 * @name Javier Vargas Garcia
 * @date 06/06/2022
 */

#ifndef COMPONENTS_MYESP_COMPONENTS_TONE_INCLUDE_NOTE_H_
#define COMPONENTS_MYESP_COMPONENTS_TONE_INCLUDE_NOTE_H_

/**
 * Enumeration with frequencies.
 */
typedef enum {
	note_none = 0, 	///< Use this value as a pause
	note_B0 = 31,   ///< note_B0
	note_C1 = 33,   ///< note_C1
	note_CS1 = 35,  ///< note_CS1
	note_D1 = 37,   ///< note_D1
	note_DS1 = 39,  ///< note_DS1
	note_E1 = 41,   ///< note_E1
	note_F1 = 44,   ///< note_F1
	note_FS1 = 46,  ///< note_FS1
	note_G1 = 49,   ///< note_G1
	note_GS1 = 52,  ///< note_GS1
	note_A1 = 55,   ///< note_A1
	note_AS1 = 58,  ///< note_AS1
	note_B1 = 62,   ///< note_B1
	note_C2 = 65,   ///< note_C2
	note_CS2 = 69,  ///< note_CS2
	note_D2 = 73,   ///< note_D2
	note_DS2 = 78,  ///< note_DS2
	note_E2 = 82,   ///< note_E2
	note_F2 = 87,   ///< note_F2
	note_FS2 = 93,  ///< note_FS2
	note_G2 = 98,   ///< note_G2
	note_GS2 = 104, ///< note_GS2
	note_A2 = 110,  ///< note_A2
	note_AS2 = 117, ///< note_AS2
	note_B2 = 123,  ///< note_B2
	note_C3 = 131,  ///< note_C3
	note_CS3 = 139, ///< note_CS3
	note_D3 = 147,  ///< note_D3
	note_DS3 = 156, ///< note_DS3
	note_E3 = 165,  ///< note_E3
	note_F3 = 175,  ///< note_F3
	note_FS3 = 185, ///< note_FS3
	note_G3 = 196,  ///< note_G3
	note_GS3 =208,  ///< note_GS3
	note_A3 = 220,  ///< note_A3
	note_AS3 = 233, ///< note_AS3
	note_B3 = 247,  ///< note_B3
	note_C4 = 262,  ///< note_C4
	note_CS4 = 277, ///< note_CS4
	note_D4 = 294,  ///< note_D4
	note_DS4 = 311, ///< note_DS4
	note_E4 = 330,  ///< note_E4
	note_F4 = 349,  ///< note_F4
	note_FS4 = 370, ///< note_FS4
	note_G4 = 392,  ///< note_G4
	note_GS4 = 415, ///< note_GS4
	note_A4 = 440,  ///< note_A4
	note_AS4 = 466, ///< note_AS4
	note_B4 = 494,  ///< note_B4
	note_C5 = 523,  ///< note_C5
	note_CS5 = 554, ///< note_CS5
	note_D5 = 587,  ///< note_D5
	note_DS5 = 622, ///< note_DS5
	note_E5 = 659,  ///< note_E5
	note_F5 = 698,  ///< note_F5
	note_FS5 = 740, ///< note_FS5
	note_G5 = 784,  ///< note_G5
	note_GS5 = 831, ///< note_GS5
	note_A5 = 880,  ///< note_A5
	note_AS5 = 932, ///< note_AS5
	note_B5 = 988,  ///< note_B5
	note_C6 = 1047, ///< note_C6
	note_CS6 = 1109,///< note_CS6
	note_D6 = 1175, ///< note_D6
	note_DS6 = 1245,///< note_DS6
	note_E6 = 1319, ///< note_E6
	note_F6 = 1397, ///< note_F6
	note_FS6 = 1480,///< note_FS6
	note_G6 = 1568, ///< note_G6
	note_GS6 = 1661,///< note_GS6
	note_A6 = 1760, ///< note_A6
	note_AS6 = 1865,///< note_AS6
	note_B6 = 1976, ///< note_B6
	note_C7 = 2093, ///< note_C7
	note_CS7 = 2217,///< note_CS7
	note_D7 = 2349, ///< note_D7
	note_DS7 = 2489,///< note_DS7
	note_E7 = 2637, ///< note_E7
	note_F7 = 2794, ///< note_F7
	note_FS7 = 2960,///< note_FS7
	note_G7 = 3136, ///< note_G7
	note_GS7 = 3322,///< note_GS7
	note_A7 = 3520, ///< note_A7
	note_AS7 = 3729,///< note_AS7
	note_B7 = 3951, ///< note_B7
	note_C8 = 4186, ///< note_C8
	note_CS8 = 4435,///< note_CS8
	note_D8 = 4699, ///< note_D8
	note_DS8 = 4978,///< note_DS8
} note_t;

#endif /* COMPONENTS_MYESP_COMPONENTS_TONE_INCLUDE_NOTE_H_ */
