# myesp_components

Useful components for ESP32

## Usage

Add the following line in the top-level CMakeList.txt

```Makefile
EXTRA_COMPONENT_DIRS := components/myesp_components
```