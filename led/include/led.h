/**
 * The MIT License (see LICENSE.txt)
 *
 * @file led.h
 *
 * @brief Module for controlling leds through ESP32 LEDC hardware.
 *
 * @name Javier Vargas Garcia
 * @date 07/06/2022
 */

#ifndef COMPONENTS_MYESP_COMPONENTS_LED_INCLUDE_LED_H_
#define COMPONENTS_MYESP_COMPONENTS_LED_INCLUDE_LED_H_

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "driver/gpio.h"
#include "esp_err.h"

/**
 * Block time forever (wait time does not expires)
 */
#define LED_MS_BLOCK 0xFFFFFFFFUL

/**
 * Forward declarations
 */
typedef struct led_handler_t led_handler_t;

/**
 * LED handler
 */
typedef led_handler_t* led_handler;

/**
 * Task mode error callback function
 */
typedef void (*led_task_error_cb)(led_handler led, esp_err_t err);

/**
 * Task configuration structure
 */
typedef struct {
	char* name; ///< String with the name of the task.
	int priority; ///< Priority of the task.
	BaseType_t cpu; ///< CPU where task must run.
	uint32_t queue_elements; ///< Maximum queued elements for sending commands.
	led_task_error_cb error_cb; ///< Error callback to be executed from the task when arise an error (NULL to ignore).
} led_task_config_t;

/**
 * @brief Initialize a led GPIO.
 *
 * Use this function to create a led handler.
 *
 * @param led[out]		Pointer to create a link handler.
 * @param gpio[in]		Output GPIO number.
 * @param channel[in]	LEDC channel to be used for this led control.
 *
 * @return 	ESP_OK if success or standard ESP error code otherwise.
 */
esp_err_t led_init(led_handler* led, gpio_num_t gpio, ledc_channel_t channel, bool level_on);

/**
 * @brief Initialize a task to handle a set of leds.
 *
 * Use this function to create a FreeRTOS task and attach a set of led handlers to be controlled using an specified LEDC timer.
 * All leds must be previously initialized with led_init function.
 *
 * @param task_config[in]	Task configuration parameters.
 * @param timer[in]			LEDC timer number.
 * @param leds[in]			Led handler array to be attached.
 * @param led_size[in]		Size of led handler array (leds) in bytes.
 *
 * @return 	ESP_OK if success or standard ESP error code otherwise.
 */
esp_err_t led_task_init(const led_task_config_t* task_config, ledc_timer_t timer, led_handler* leds, size_t leds_size);

/**
 * @brief Turn on the led.
 *
 * @param led[in]			Led handler.
 * @param block_ms[in]		Blocking time in milliseconds. Use LED_MS_BLOCK to block forever.
 *
 * @return	ESP_OK if success, ESP_ERR_TIMEOUT if block time expires or standard ESP error code otherwise.
 */
esp_err_t led_on(led_handler led, uint32_t block_ms);

/**
 * @brief Turn off the led.
 *
 * @param led[in]			Led handler.
 * @param block_ms[in]		Blocking time in milliseconds. Use LED_MS_BLOCK to block forever.
 *
 * @return	ESP_OK if success, ESP_ERR_TIMEOUT if block time expires or standard ESP error code otherwise.
 */
esp_err_t led_off(led_handler led, uint32_t block_ms);

/**
 * @brief Blink the led.
 *
 * @param led[in]			Led handler.
 * @param period_ms[in]		Blink period in milliseconds.
 * @param fade[in]			Blink fade mode.
 * @param block_ms[in]		Blocking time in milliseconds. Use LED_MS_BLOCK to block forever.
 *
 * @return	ESP_OK if success, ESP_ERR_TIMEOUT if block time expires or standard ESP error code otherwise.
 */
esp_err_t led_blink(led_handler led, uint32_t period_ms, bool fade, uint32_t block_ms);

/**
 * @brief Pulse the led.
 *
 * @param led[in]			Led handler.
 * @param period_ms[in]		Blink period in milliseconds.
 * @param count[in]			Number of pulses.
 * @param fade[in]			Blink fade mode.
 * @param block_ms[in]		Blocking time in milliseconds. Use LED_MS_BLOCK to block forever.
 *
 * @return	ESP_OK if success, ESP_ERR_TIMEOUT if block time expires or standard ESP error code otherwise.
 */
esp_err_t led_pulse(led_handler led, uint32_t period_ms, uint32_t count, bool fade, uint32_t block_ms);

#endif /* COMPONENTS_MYESP_COMPONENTS_LED_INCLUDE_LED_H_ */
