/**
 * The MIT License (see LICENSE.txt)
 *
 * @file led.c
 *
 * @brief Led module source code.
 *
 * @name Javier Vargas Garcia
 * @date 07/06/2022
 */

#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "led.h"

/* Static configuration */

#define LEDC_SPEED_MODE 		LEDC_HIGH_SPEED_MODE
#define LEDC_DUTY_RESOLUTION 	LEDC_TIMER_8_BIT
#define LEDC_CLK				LEDC_AUTO_CLK
#define LEC_FREQ_HZ				10000UL
#define TASK_STACK_SIZE			2048UL
#define LEDC_DUTY_ON			((1UL<<LEDC_DUTY_RESOLUTION)-1UL)
#define LEDC_DUTY_OFF			0UL

#define LED_TICKS_TO_MS(ms) 	(ms == LED_MS_BLOCK ? portMAX_DELAY : pdMS_TO_TICKS(ms))

/* Structures and enumerations */

typedef enum {
	led_mode_manual,
	led_mode_blink,
	led_mode_pulse,
} led_mode_t;

typedef struct {
	led_mode_t mode;
	uint32_t tick_period;
	uint32_t count;
	bool fade;
	bool level;
} led_state_t;

typedef struct {
	led_handler led;
	led_state_t state_request;
} led_queue_t;

typedef struct {
	led_handler* leds;
	uint32_t leds_count;
	TaskHandle_t task;
	QueueHandle_t queue;
	led_task_error_cb error_cb;
} led_task_handler_t;

typedef led_task_handler_t* led_task_handler;

struct led_handler_t {
	gpio_num_t gpio;
	ledc_channel_t channel;
	led_task_handler task_handler;
	TickType_t tick;
	bool level_on;
	bool level_default;
	led_state_t state_0;
	led_state_t state_1;
	led_state_t* state_current;
	led_state_t* state_request;
	bool request;
};

/* Private functions */

static esp_err_t led_queue_send(led_handler led, led_queue_t* led_queue, uint32_t block_ms) {
	esp_err_t err = ESP_ERR_INVALID_ARG;
	BaseType_t ret_;
	if (led != NULL && led->task_handler != NULL) {
		/* Send command to queue task */
		do {
			ret_ = xQueueSend(led->task_handler->queue, led_queue, LED_TICKS_TO_MS(block_ms));
		} while (ret_ != pdTRUE && block_ms == LED_MS_BLOCK);
		if (ret_ != pdTRUE) err = ESP_ERR_TIMEOUT;
	}

	return err;
}

static void led_update(led_handler led) {
	esp_err_t err;
	uint32_t duty = led->state_current->level == led->level_on ? LEDC_DUTY_ON : LEDC_DUTY_OFF;
	/* Fade mode */
	if (led->state_current->fade) err = ledc_set_fade_time_and_start(LEDC_SPEED_MODE, led->channel, duty, led->state_current->tick_period * portTICK_RATE_MS, LEDC_FADE_NO_WAIT);
	/* Normal mode */
	else err = ledc_set_duty_and_update(LEDC_SPEED_MODE, led->channel, duty, 0);
	/* Report errors */
	if (err && led->task_handler->error_cb != NULL) led->task_handler->error_cb(led, err);
}

static void led_task(void* arg) {
	BaseType_t data_available;
	led_task_handler task_handler = (led_task_handler)arg;
	led_handler led;
	led_state_t* led_state_tmp;
	led_queue_t led_queue;
	uint32_t tick_diff;
	TickType_t tick = xTaskGetTickCount();
	TickType_t tick_wait = portMAX_DELAY;
	bool available;

	for (;;) {

        /* Wait in queue */
        data_available = xQueueReceive(task_handler->queue, &led_queue, tick_wait);
        tick_wait = portMAX_DELAY;

        /* Update tick */
        tick = xTaskGetTickCount();

        /* Process incoming queue messages */
        if (data_available == pdTRUE) {
        	led = led_queue.led;
        	/* Set default level (request might be overwritten but default level is captured) */
			if (led_queue.state_request.mode == led_mode_manual) led->level_default = led_queue.state_request.level;
        	/* Set the request state */
        	led->request = true;
        	memcpy(led->state_request, &led_queue.state_request, sizeof(led_state_t));
        }

		/*  Led control polling */
		for (int i=0; i<task_handler->leds_count; i++) {
			led = task_handler->leds[i];

			/* Get available status (period expired or manual mode active) */
			if (led->state_current->mode != led_mode_manual) {
				/* Check that tick has expired with overflow handling (tick >= led->tick) */
				if ((volatile uint32_t)(tick + led->state_current->tick_period) - led->tick >= (volatile uint32_t)led->state_current->tick_period) available = true;
				else available = false;
			}
			else available = true;

			/* Led update available */
			if (available) {
				/* Update status from the request */
				if (led->request) {
					led->request = false;
					/* Swap pointers */
					led_state_tmp = led->state_current;
					led->state_current = led->state_request;
					led->state_request = led_state_tmp;
					/* Manual mode, update led */
					if (led->state_current->mode == led_mode_manual) led_update(led);
					/* Keep the level for periodic modes */
					else led->state_current->level = led->state_request->level;
				}

				/* Periodic mode */
				if (led->state_current->mode != led_mode_manual)
				{
					/* Change led state */
					led->state_current->level = !led->state_current->level;
					led_update(led);

					/* Set the next updating tick */
					led->tick = tick + led->state_current->tick_period;

					/* Pulse mode, go to manual after count down */
					if (led->state_current->mode == led_mode_pulse && led->state_current->level == led->level_default) {
						if (--(led->state_current->count) == 0) led->state_current->mode = led_mode_manual;
					}
				}
			}

			/* Compute tick wait */
			if (led->state_current->mode != led_mode_manual) {
				/* Check the nearest tick_diff to wake up */
				tick_diff = led->tick > tick ? led->tick - tick : 0;
				if (tick_diff < tick_wait) tick_wait = tick_diff;
			}
		}
	}
}

/* Functions */

esp_err_t led_init(led_handler* led, gpio_num_t gpio, ledc_channel_t channel, bool level_on) {
	esp_err_t err = ESP_FAIL;
	led_handler handler = NULL;

	/* Check input values */
	if (led != NULL) {

		/* Create handler in heap */
		handler = malloc(sizeof(led_handler_t));
		if (handler != NULL) {
			memset(handler, 0, sizeof(led_handler_t));

			/* Fill handler information */
			handler->channel = channel;
			handler->gpio = gpio;
			handler->level_on = level_on;

			/* Default location for current and requested status */
			handler->state_current = &handler->state_0;
			handler->state_request = &handler->state_1;

			err = ESP_OK;
		}
	}

	*led = handler;
	return err;
}

esp_err_t led_task_init(const led_task_config_t* task_config, ledc_timer_t timer, led_handler* leds, size_t leds_size) {
	esp_err_t err = ESP_FAIL;
	led_task_handler task_handler = NULL;

	if (task_config == NULL || leds == NULL || leds_size == 0 || leds_size % sizeof(led_handler)) goto task_init_end;

	/* Create handler in heap */
	task_handler = malloc(sizeof(led_task_handler_t));
	if (task_handler == NULL) goto task_init_end;
	memset(task_handler, 0, sizeof(led_task_handler_t));

	/* Add memory for led handler array and copy them */
	task_handler->leds = malloc(leds_size);
	if (task_handler->leds == NULL) goto task_init_end;
	memcpy(task_handler->leds, leds, leds_size);
	task_handler->leds_count = leds_size / sizeof(led_handler);

	/* Setup LEDC structures */
	ledc_timer_config_t timer_config = {
			.speed_mode = LEDC_SPEED_MODE,
			.duty_resolution = LEDC_DUTY_RESOLUTION,
			.clk_cfg = LEDC_CLK,
			.freq_hz = LEC_FREQ_HZ,
			.timer_num = timer,
	};
	ledc_channel_config_t channel_config = {
			.speed_mode = LEDC_SPEED_MODE,
			.intr_type = LEDC_INTR_DISABLE,
			.timer_sel = timer,
			.duty = 0,
			.hpoint = 0,
	};

	/* Install fade functionality */
	ledc_fade_func_install(0);

	/* Initialize LEDC Timer */
	err = ledc_timer_config(&timer_config);
	if (err) goto task_init_end;

	/* Initialize LEDC Channels */
	for (int i=0; i<task_handler->leds_count; i++) {
		if (task_handler->leds[i] == NULL) { err = ESP_FAIL; goto task_init_end; }
		channel_config.channel = task_handler->leds[i]->channel;
		channel_config.gpio_num = task_handler->leds[i]->gpio;
		err = ledc_channel_config(&channel_config);
		if (err) goto task_init_end;
		/* Update idle led level */
		err = ledc_set_duty_and_update(LEDC_SPEED_MODE, channel_config.channel, (!task_handler->leds[i]->level_on) ? LEDC_DUTY_ON : LEDC_DUTY_OFF, 0);
		if (err) goto task_init_end;
	}

	/* Error callback (optional) */
	task_handler->error_cb = task_config->error_cb;

	/* Create queue */
	task_handler->queue = xQueueCreate(task_config->queue_elements, sizeof(led_queue_t));
	if (task_handler->queue == NULL) { err = ESP_FAIL; goto task_init_end; }

	/* Create task */
	if (xTaskCreatePinnedToCore(led_task, task_config->name,
			TASK_STACK_SIZE, task_handler, task_config->priority,
			&task_handler->task, task_config->cpu) == pdTRUE) err = ESP_OK;

task_init_end:

	/* Free the resources if there is an error */
	if (err && task_handler != NULL) {
		if (task_handler->leds != NULL) free(task_handler->leds);
		if (task_handler->queue != NULL) vQueueDelete(task_handler->queue);
		free(task_handler);
		task_handler = NULL;
	}

	/* Save task handler within each led structure (user does not need to keep track of it) */
	for (int i=0; i<task_handler->leds_count; i++) task_handler->leds[i]->task_handler = task_handler;

	return err;
}

esp_err_t led_on(led_handler led, uint32_t block_ms) {
	esp_err_t err;
	led_queue_t led_queue = {.led = led,
			.state_request = {.mode = led_mode_manual, .level = true}};
	err = led_queue_send(led, &led_queue, block_ms);
	return err;
}

esp_err_t led_off(led_handler led, uint32_t block_ms) {
	esp_err_t err;
	led_queue_t led_queue = {.led = led,
			.state_request = {.mode = led_mode_manual, .level = false}};
	err = led_queue_send(led, &led_queue, block_ms);
	return err;
}

esp_err_t led_blink(led_handler led, uint32_t period_ms, bool fade, uint32_t block_ms) {
	esp_err_t err = ESP_ERR_INVALID_ARG;
	uint32_t tick_period = pdMS_TO_TICKS(period_ms)/2;
	if (tick_period > 0) {
		led_queue_t led_queue = {.led = led,
				.state_request = {.mode = led_mode_blink, .tick_period = tick_period, .fade = fade}};
		err = led_queue_send(led, &led_queue, block_ms);
	}
	return err;
}

esp_err_t led_pulse(led_handler led, uint32_t period_ms, uint32_t count, bool fade, uint32_t block_ms) {
	esp_err_t err = ESP_ERR_INVALID_ARG;
	uint32_t tick_period = pdMS_TO_TICKS(period_ms)/2;
	if (count > 0 && tick_period > 0) {
		led_queue_t led_queue = {.led = led,
				.state_request = {.mode = led_mode_pulse, .tick_period = tick_period, .count = count, .fade = fade}};
		err = led_queue_send(led, &led_queue, block_ms);
	}
	return err;
}
