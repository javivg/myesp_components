/**
 * The MIT License (see LICENSE.txt)
 *
 * @file eport.h
 *
 * @brief EPORT communication API for a multi-tasking and event-driven programming.
 *
 * @name Javier Vargas Garcia
 * @date 27/12/2021
 */

#ifndef COMPONENTS_MYESP_COMPONENTS_EPORT_EPORT_H_
#define COMPONENTS_MYESP_COMPONENTS_EPORT_EPORT_H_

#include <stdio.h>
#include <stdbool.h>

/* Return type */
typedef enum {
	eport_ok = 0,
	eport_err = 1,
} eport_ret_t;

/* Block time forever */
#define EPORT_MS_BLOCK 0xffffffffUL

/* Structures */
typedef struct eport_handler_t eport_handler_t;
typedef struct eport_buffer_handler_t eport_buffer_handler_t;
typedef struct eport_timer_handler_t eport_timer_handler_t;

/* Handlers */
typedef eport_handler_t* eport_handler;
typedef eport_buffer_handler_t* eport_buffer_handler;
typedef eport_timer_handler_t* eport_timer_handler;

/* Package lost callback */
typedef void (*eport_buffer_pkglost_cb)(eport_handler eport, int evt);



/******************************/
/********** EPORTS ************/
/******************************/



/**
 * @brief Create and initialize an EPORT.
 *
 * @param buffer[in] 	BUFFER attached to this EPORT that will receive incoming events.
 *
 * @return	EPORT handler.
 */
eport_handler eport_init(eport_buffer_handler buffer);

/**
 * @brief Set a connection between two EPORTS.
 *
 * All incoming events to an EPORT are redirected to a second EPORT with an optional offset.
 *
 * @param eport[in] 		Source EPORT handler.
 * @param eport_dest[in] 	Destination EPORT handler.
 * @param evt_offset[in] 	Event offset to apply -> evt_dest = evt_src + evt_offset.
 *
 * @note	This connection must be done prior any EPORT usage.
 * 			It is not thread safe so user shall make all the connections sequentially at initialization.
 *
 * @return	EPORT eport_ok (0) if succeed or eport_err (1) if fail.
 */
eport_ret_t eport_connect(eport_handler eport, eport_handler eport_dest, int evt_offset);

/**
 * @brief Set a connection between two EPORTS for a specific event.
 *
 * User specified incoming event from an EPORT is redirected and translated to a second EPORT event.
 *
 * @param eport[in] 		Source EPORT handler.
 * @param eport_dest[in] 	Destination EPORT handler.
 * @param evt[in] 			Source event number to connect.
 * @param evt_dest[in]		Destination event number to receive.
 *
 * @note	This connection must be done prior any EPORT usage.
 * 			It is not thread safe so user shall make all the connections sequentially at initialization.
 *
 * @return	EPORT eport_ok (0) if succeed or eport_err (1) if fail.
 */
eport_ret_t eport_connectEVT(eport_handler eport, eport_handler eport_dest, int evt, int evt_dest);

/**
 * @brief Send an event and message through an EPORT.
 *
 * Event and message are sent to the BUFFER attached to this EPORT with a maximum block time.
 * If apply, connected EPORTs receive this event (with an offset or translated).
 *
 * @param eport[in] 		EPORT handler.
 * @param evt[in] 			Event number to send.
 * @param msg_p[in] 		Pointer to message to send.
 * @param msg_size[in]		Message size in bytes.
 * @param send_block_ms[in]	Send block time in milliseconds (with OS ticks precision).
 * 							Use EPORT_MS_BLOCK to block without limit time.
 *
 * @note	This function is thread safe and can be used from an ISR.
 * 			Sending block time (send_block_ms) can be applied more than once when there are
 * 			multiple EPORT connections with BUFFERs.
 *
 * @return	EPORT eport_ok (0) if succeed or eport_err (1) if fail.
 */
eport_ret_t eport_sendMSG(eport_handler eport, int evt, void* msg_p, size_t msg_size, uint32_t send_block_ms);

/**
 * @brief Send an event through an EPORT.
 *
 * Event is sent to the BUFFER attached to this EPORT with a maximum block time.
 * If apply, connected EPORTs receive this event (with an offset or translated).
 *
 * @param eport[in] 		EPORT handler.
 * @param evt[in] 			Event number to send.
 * @param send_block_ms[in]	Send block time in milliseconds (with OS ticks precision).
 * 							Use EPORT_MS_BLOCK to block without limit time.
 *
 * @note	This function is thread safe and can be used from an ISR.
 * 			Sending block time (send_block_ms) can be applied more than once when there are
 * 			multiple EPORT connections with BUFFERs.
 *
 * @return	EPORT eport_ok (0) if succeed or eport_err (1) if fail.
 */
eport_ret_t eport_sendEVT(eport_handler eport, int evt, uint32_t send_block_ms);



/******************************/
/********** BUFFERS ***********/
/******************************/



/**
 * @brief Create and initialize a BUFFER.
 *
 * This BUFFER will receive incoming packets (events and messages) from attached EPORT.
 * User shall attach this BUFFER to an EPORT with eport_init() function.
 *
 * @param buffer_size[in] 	BUFFER size in bytes.
 * @param pkglost_cb[in] 	User callback to notify incoming packets lost (timed out).
 *
 * @note	Callback is executed from sender function.
 * 			Any additional action here will affect to sender.
 *
 * @return	BUFFER handler.
 */
eport_buffer_handler eport_buffer_init(size_t buffer_size, eport_buffer_pkglost_cb pkglost_cb);

/**
 * @brief Wait for BUFFER incoming packets (events and messages).
 *
 * This function will block until packet is coming or timeout is exceeded.
 *
 * @param buffer[in] 		BUFFER handler.
 * @param evt[out] 			Event received. Set NULL if not used.
 * @param msg_p[out]		Message received. Set NULL if not used.
 * @param msg_size[out]		Message size received. Set NULL if not used.
 * @param send_block_ms[in]	Wait block time in milliseconds (with OS ticks precision).
 * 							Use EPORT_MS_BLOCK to block without limit time.
 *
 * @note	Do not set multiple threads waiting for the same BUFFER.
 *
 * @return	Sender EPORT handler. Note that sender might not match attached EPORT when there are connections.
 */
eport_handler eport_buffer_wait(eport_buffer_handler buffer, int* evt, void** msg_p, size_t* msg_size, uint32_t wait_block_ms);



/******************************/
/********** TIMERS ************/
/******************************/



/**
 * @brief Create and initialize a TIMER.
 *
 * This TIMER will send event and message to an EPORT.
 *
 * @param eport[in] 		EPORT handler attached to this TIMER.
 * @param autoreload[in] 	Auto reload mode to start again when time expires.
 *
 * @note	Timer will be tied to a OS Timer task.
 * 			Consider to set Timer task priority to maximum in order to
 * 			avoid context switching before deliver the message to all
 * 			EPORTs connections.
 *
 * @return	TIMER handler.
 */
eport_timer_handler eport_timer_init(eport_handler eport, bool autoreload);

/**
 * @brief Attach an event and message to a TIMER.
 *
 * Set the event and message you want to send with this TIMER.
 *
 * @param timer[in] 		TIMER handler.
 * @param evt[in] 			Event number to send.
 * @param msg_p[in] 		Pointer to message to send.
 * @param msg_size[in]		Message size in bytes.
 * @param send_block_ms[in]	Send block time in milliseconds for TIMER (with OS ticks precision).
 * 							Use EPORT_MS_BLOCK to block without limit time.
 *
 * @note	This function is thread safe and can be used from an ISR.
 * 			Sending block time (send_block_ms) can be applied more than once when there are
 * 			multiple EPORT connections with BUFFERs.
 *
 * @return	EPORT eport_ok (0) if succeed or eport_err (1) if fail.
 */
eport_ret_t eport_timer_attachMSG(eport_timer_handler timer, int evt, void* msg_p, size_t msg_size, uint32_t send_block_ms);

/**
 * @brief Attach an event to a TIMER.
 *
 * Set the event you want to send with this TIMER.
 *
 * @param timer[in] 		TIMER handler.
 * @param evt[in] 			Event number to send.
 * @param send_block_ms[in]	Send block time in milliseconds from TIMER task (with OS ticks precision).
 * 							Use EPORT_MS_BLOCK to block without limit time.
 *
 * @note	This function is thread safe and can be used from an ISR.
 * 			Sending block time (send_block_ms) can be applied more than once when there are
 * 			multiple EPORT connections with BUFFERs.
 *
 * @return	EPORT eport_ok (0) if succeed or eport_err (1) if fail.
 */
eport_ret_t eport_timer_attachEVT(eport_timer_handler timer, int evt, uint32_t send_block_ms);

/**
 * @brief Send TIMER packet manually once.
 *
 * @param timer[in] 	TIMER handler.
 *
 * @note	This function is thread safe and can be used from an ISR.
 * 			Sending block time (send_block_ms) attached with message and event will be used.
 * 			Sending block time can be applied more than once when there are
 * 			multiple EPORT connections with BUFFERs.
 *
 * @return	EPORT eport_ok (0) if succeed or eport_err (1) if fail.
 */
eport_ret_t eport_timer_send(eport_timer_handler timer);

/**
 * @brief Start TIMER.
 *
 * @param timer[in] 			TIMER handler.
 * @param ms[in] 				Period in milliseconds.
 * @param timer_block_ms[in]	Block time in milliseconds to wait TIMER task to receive start command (with OS ticks precision).
 * 								Use EPORT_MS_BLOCK to block without limit time.
 *
 * @note	This function is thread safe and can be used from an ISR.
 *
 * @return	EPORT eport_ok (0) if succeed or eport_err (1) if fail.
 */
eport_ret_t eport_timer_start(eport_timer_handler timer, uint32_t ms, uint32_t timer_block_ms);

/**
 * @brief Restart TIMER.
 *
 * @param timer[in] 			TIMER handler.
 * @param timer_block_ms[in]	Block time in milliseconds to wait TIMER task to receive restart command (with OS ticks precision).
 * 								Use EPORT_MS_BLOCK to block without limit time.
 *
 * @note	This function is thread safe and can be used from an ISR.
 *
 * @return	EPORT eport_ok (0) if succeed or eport_err (1) if fail.
 */
eport_ret_t eport_timer_restart(eport_timer_handler timer, uint32_t timer_block_ms);

/**
 * @brief Stop TIMER.
 *
 * @param timer[in] 			TIMER handler.
 * @param timer_block_ms[in]	Block time in milliseconds to wait TIMER task to receive stop command (with OS ticks precision).
 * 								Use EPORT_MS_BLOCK to block without limit time.
 *
 * @note	This function is thread safe and can be used from an ISR.
 *
 * @return	EPORT eport_ok (0) if succeed or eport_err (1) if fail.
 */
eport_ret_t eport_timer_stop(eport_timer_handler timer, uint32_t timer_block_ms);

#endif /* COMPONENTS_MYESP_COMPONENTS_EPORT_EPORT_H_ */
