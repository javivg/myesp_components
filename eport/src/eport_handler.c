/**
 * The MIT License (see LICENSE.txt)
 *
 * @file eport_handler.c
 *
 * @brief Handler functions source code.
 *
 * @name Javier Vargas Garcia
 * @date 27/12/2021
 */

#include <string.h>
#include <stdlib.h>

#include "eport_private.h"

typedef enum {
	eport_connect_offset,
	eport_connect_evt,
} eport_connect_mode_t;

typedef struct {
	eport_connect_mode_t mode;	// Connection mode
	eport_handler eport;		// Eport destination
	int evt1;					// Event 1 filter
	int evt2;					// Event 2 filter
} eport_connect_t;

/* Event port handler structure */
struct eport_handler_t {
	eport_buffer_handler buffer;	// Buffer attached
	eport_connect_t* connect;			// Connections array
	int connect_count;				// Connections count
};

////////////////////////////////////
///////////// STATIC ///////////////
////////////////////////////////////

eport_ret_t eport_connect_(eport_handler eport, eport_handler eport_dest, eport_connect_mode_t mode, int evt1, int evt2) {
	eport_ret_t ret = eport_err;

	if (eport != NULL && eport_dest != NULL) {
		/* Add memory for a new connection */
		eport_connect_t* new_connect = realloc(eport->connect, (eport->connect_count+1)*sizeof(eport_connect_t));
		if (new_connect != NULL) {
			eport->connect= new_connect;
			eport_connect_t* this_connect = &eport->connect[eport->connect_count];

			/* Set connection parameters */
			this_connect->eport = eport_dest;
			this_connect->mode = mode;
			this_connect->evt1 = evt1;
			this_connect->evt2 = evt2;
			eport->connect_count++;

			ret = eport_ok;
		}
	}
	return ret;
}

////////////////////////////////////
//////////////// API ///////////////
////////////////////////////////////

eport_handler eport_init(eport_buffer_handler buffer) {
	eport_handler eport = NULL;

	/* Create handler in heap */
	eport = malloc(sizeof(eport_handler_t));
	if (eport != NULL) {
		memset(eport, 0, sizeof(eport_handler_t));
		eport->buffer = buffer;
	}

	/* Return handler */
	return eport;
}

eport_ret_t eport_connect(eport_handler eport, eport_handler eport_dest, int evt_offset) {
	return eport_connect_(eport, eport_dest, eport_connect_offset, evt_offset, 0);
}

eport_ret_t eport_connectEVT(eport_handler eport, eport_handler eport_dest, int evt, int evt_dest) {
	return eport_connect_(eport, eport_dest, eport_connect_evt, evt, evt_dest);
}

eport_ret_t eport_sendMSG(eport_handler eport, int evt, void* msg_p, size_t msg_size, uint32_t send_block_ms) {
	eport_ret_t ret = eport_err;
	/* Create packet in stack */
	EPORT_PKG_NEW(pkg, eport, evt, msg_p, msg_size);
	/* Send */
	if (eport != NULL) ret = eport_send(eport, (eport_pkg_t*)&pkg, sizeof(pkg), send_block_ms);
	return ret;
}

eport_ret_t eport_sendEVT(eport_handler eport, int evt, uint32_t send_block_ms) {
	return eport_sendMSG(eport, evt, NULL, 0, send_block_ms);
}

////////////////////////////////////
////////////// PRIVATE /////////////
////////////////////////////////////

eport_ret_t eport_send(eport_handler eport, eport_pkg_t* pkg, size_t pkg_size, uint32_t send_block_ms) {
	eport_ret_t ret = eport_ok;

	/* BUFFER attached */
	if (eport->buffer != NULL) ret = eport_buffer_send(eport->buffer, pkg, pkg_size, send_block_ms);

	/* EPORT connections */
	for (int idx = 0; idx < eport->connect_count; idx++) {
		eport_connect_t* this_connect = &eport->connect[idx];

		/* Save current event (modified during connections) */
		int evt_backup = pkg->evt;

		/* Connection mode */
		switch (this_connect->mode) {

		/* Connect packet with an offset */
		case eport_connect_offset:
			pkg->evt += this_connect->evt1;
			ret |= eport_send(this_connect->eport, pkg, pkg_size, send_block_ms);
			break;

		/* Connect packet when input event matches and translate output event */
		case eport_connect_evt:
			if (pkg->evt == this_connect->evt1) {
				pkg->evt = this_connect->evt2;
				ret |= eport_send(this_connect->eport, pkg, pkg_size, send_block_ms);
			}
			break;

		default:
			break;
		}

		/* Restore event */
		pkg->evt = evt_backup;
	}

	return ret;
}
