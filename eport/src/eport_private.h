/**
 * The MIT License (see LICENSE.txt)
 *
 * @file eport_private.h
 *
 * @brief Private header with functions and type definitions.
 *
 * @name Javier Vargas Garcia
 * @date 27/12/2021
 */

#ifndef COMPONENTS_MYESP_COMPONENTS_EPORT_SRC_EPORT_PRIVATE_H_
#define COMPONENTS_MYESP_COMPONENTS_EPORT_SRC_EPORT_PRIVATE_H_

#include "eport.h"

/////////////////////////////////////
//////// PRIVATE DEFINITIONS ////////
/////////////////////////////////////

#define EPORT_TICKS_TO_MS(ms) (ms == EPORT_MS_BLOCK ? portMAX_DELAY : pdMS_TO_TICKS(ms))

/* PACKAGE LAYOUT */
typedef struct {
	eport_handler eport;
	int evt;
	uint8_t msg[];
} eport_pkg_t;

/* Variable size packet structure layout */
#define EPORT_PKG_SIZED_T(msg_size) struct {eport_handler eport; int evt; uint8_t msg[(msg_size)]; }

/* Header size */
#define EPORT_PKG_SIZE_HEADER (sizeof(eport_handler) + sizeof(int))

/* Set values to a packet pointer */
#define EPORT_PKG_SET(pkg_p, eport_, evt_, msg_, msg_size) 		\
		(pkg_p)->eport = (eport_);								\
		(pkg_p)->evt = (evt_);									\
		memcpy((pkg_p)->msg, (msg_), (msg_size))

/* Create and initialize a new packet in stack */
#define EPORT_PKG_NEW(pkg, eport_, evt_, msg_, msg_size) 			\
		EPORT_PKG_SIZED_T(msg_size) pkg;							\
		EPORT_PKG_SET(&pkg, (eport_), (evt_), (msg_), (msg_size))

/////////////////////////////////////
///////// PRIVATE FUNCTIONS /////////
/////////////////////////////////////

/* HANDLER private functions */
eport_ret_t eport_send(eport_handler eport, eport_pkg_t* pkg, size_t pkg_size, uint32_t send_block_ms);

/* BUFFER private functions */
eport_ret_t eport_buffer_send(eport_buffer_handler buffer, eport_pkg_t* pkg, size_t pkg_size, uint32_t send_block_ms);

#endif /* COMPONENTS_MYESP_COMPONENTS_EPORT_SRC_EPORT_PRIVATE_H_ */
