/**
 * The MIT License (see LICENSE.txt)
 *
 * @file eport_timer.c
 *
 * @brief Timer functions source code.
 *
 * @name Javier Vargas Garcia
 * @date 27/12/2021
 */

#include <string.h>
#include "freertos/Freertos.h"
#include "freertos/timers.h"
#include "freertos/semphr.h"

#include "eport_private.h"

/* Event port timer handler structure */
struct eport_timer_handler_t {
	TimerHandle_t timer;		// Timer handler
	eport_handler eport;		// Eport attached
	SemaphoreHandle_t mutex;	// Mutex handler
	eport_pkg_t* pkg;			// Pointer to packet attached
	size_t pkg_size;			// Package size
	TickType_t period_ticks;	// Ticks period
	uint32_t send_block_ms;		// Timer task send block time
	bool autoreload;			// Autoreload mode
};

////////////////////////////////////
////////////// STATIC //////////////
////////////////////////////////////

static void timer_callback(TimerHandle_t t) {
	eport_timer_handler timer = (eport_timer_handler)pvTimerGetTimerID(t);

	/* Take mutex */
	BaseType_t mutex = xPortInIsrContext() ?
			xSemaphoreTakeFromISR(timer->mutex, NULL) :
			xSemaphoreTake(timer->mutex, portMAX_DELAY);

	if (mutex == pdTRUE) {

		/* Send */
		eport_send(timer->eport, timer->pkg, timer->pkg_size, timer->send_block_ms);

		/* Return mutex */
		xPortInIsrContext() ?
				xSemaphoreGiveFromISR(timer->mutex, NULL) :
				xSemaphoreGive(timer->mutex);

	}
}

////////////////////////////////////
//////////////// API ///////////////
////////////////////////////////////

eport_timer_handler eport_timer_init(eport_handler eport, bool autoreload) {
	eport_ret_t ret = eport_err;
	eport_timer_handler timer = NULL;

	if (eport != NULL) {

		/* Allocate timer structure */
		timer = malloc(sizeof(eport_timer_handler_t));
		if (timer != NULL) {
			memset(timer, 0, sizeof(eport_timer_handler_t));

			/* Create mutex */
			timer->mutex = xSemaphoreCreateMutex();
			if (timer->mutex != NULL) {

				/* Create software timer */
				timer->timer = xTimerCreate("eport_timer", 1, autoreload, (void*)timer, &timer_callback);
				if (timer->timer != NULL) {

					timer->eport = eport;
					timer->autoreload = autoreload;
					ret = eport_ok;
				}
			}
		}
	}

	/* Free fail initialization */
	if (ret) {
		if (timer != NULL) {
			if (timer->mutex != NULL) vSemaphoreDelete(timer->mutex);
			free(timer);
			timer = NULL;
		}
	}

	/* Return timer handler */
	return timer;
}

eport_ret_t eport_timer_attachMSG(eport_timer_handler timer, int evt, void* msg_p, size_t msg_size, uint32_t send_block_ms) {
	eport_ret_t ret = eport_err;
	if (timer != NULL) {

		/* Take mutex */
		BaseType_t mutex = xPortInIsrContext() ?
				xSemaphoreTakeFromISR(timer->mutex, NULL) :
				xSemaphoreTake(timer->mutex, portMAX_DELAY);

		if (mutex == pdTRUE) {

			size_t pkg_size = sizeof(EPORT_PKG_SIZED_T(msg_size));

			/* Allocate memory */
			void* new_pkg = realloc(timer->pkg, pkg_size);
			if (new_pkg != NULL) {
				timer->pkg = new_pkg;

				/* Set packet data */
				EPORT_PKG_SET(timer->pkg, timer->eport, evt, msg_p, msg_size);
				timer->pkg_size = pkg_size;

				/* Set send block time */
				timer->send_block_ms = send_block_ms;

				ret = eport_ok;
			}

			/* Return mutex */
			xPortInIsrContext() ?
					xSemaphoreGiveFromISR(timer->mutex, NULL) :
					xSemaphoreGive(timer->mutex);
		}
	}

	return ret;
}

eport_ret_t eport_timer_attachEVT(eport_timer_handler timer, int evt, uint32_t send_block_ms) {
	return eport_timer_attachMSG(timer, evt, NULL, 0, send_block_ms);
}

eport_ret_t eport_timer_send(eport_timer_handler timer) {
	eport_ret_t ret = eport_err;
	if (timer != NULL) {

		/* Send message from timer callback */
		timer_callback(timer->timer);
		ret = eport_ok;
	}
	return ret;
}

eport_ret_t eport_timer_start(eport_timer_handler timer, uint32_t ms, uint32_t timer_block_ms) {
	eport_ret_t ret = eport_err;
	BaseType_t ret_;

	if (timer != NULL) {
		TickType_t ticks = EPORT_TICKS_TO_MS(ms);
		if (ticks != 0) {

			/* Start/restart timer with new period */
			if (xPortInIsrContext()) ret_= xTimerChangePeriodFromISR(timer->timer, ticks, NULL);
			else {
				do { ret_= xTimerChangePeriod(timer->timer, ticks, EPORT_TICKS_TO_MS(timer_block_ms));}
				while (ret_ != pdTRUE && timer_block_ms == EPORT_MS_BLOCK);
			}

			if (ret_ == pdTRUE) {

				/* Change period */
				timer->period_ticks = ticks;
				ret = eport_ok;
			}
		}
	}
	return ret;
}

eport_ret_t eport_timer_restart(eport_timer_handler timer, uint32_t timer_block_ms) {
	eport_ret_t ret = eport_err;
	BaseType_t ret_;

	if (timer != NULL) {
		if (timer->period_ticks != 0) {

			/* Start/restart timer */
			if (xPortInIsrContext()) ret_ = xTimerResetFromISR(timer->timer, NULL);
			else {
				do { ret_= xTimerReset(timer->timer, EPORT_TICKS_TO_MS(timer_block_ms)); }
				while (ret_ != pdTRUE && timer_block_ms == EPORT_MS_BLOCK);
			}
			if (ret_ == pdTRUE) ret = eport_ok;
		}
	}
	return ret;
}

eport_ret_t eport_timer_stop(eport_timer_handler timer, uint32_t timer_block_ms) {
	eport_ret_t ret = eport_err;
	BaseType_t ret_;
	if (timer != NULL) {

		/* Stop timer */
		if (xPortInIsrContext()) ret_ = xTimerStopFromISR(timer->timer, NULL);
		else {
			do { ret_ = xTimerStop(timer->timer, EPORT_TICKS_TO_MS(timer_block_ms)); }
			while (ret_ != pdTRUE && timer_block_ms == EPORT_MS_BLOCK);
		}
		if (ret_ == pdTRUE) ret = eport_ok;
	}
	return ret;
}

