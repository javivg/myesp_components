/**
 * The MIT License (see LICENSE.txt)
 *
 * @file eport_buffer.c
 *
 * @brief Buffer functions source code.
 *
 * @name Javier Vargas Garcia
 * @date 27/12/2021
 */

#include <string.h>
#include "freertos/Freertos.h"
#include "freertos/ringbuf.h"

#include "eport_private.h"

/* Event port buffer handler structure */
struct eport_buffer_handler_t {
	RingbufHandle_t buffer;				// Ring buffer handler
	eport_pkg_t* pkg;					// Current available packet pointer
	eport_buffer_pkglost_cb pkglost_cb;	// Package lost callback
};

////////////////////////////////////
//////////////// API ///////////////
////////////////////////////////////

eport_buffer_handler eport_buffer_init(size_t buffer_size, eport_buffer_pkglost_cb pkglost_cb) {
	eport_ret_t ret = eport_err;
	eport_buffer_handler buffer = NULL;

	/* Create handler in heap */
	buffer = malloc(sizeof(eport_buffer_handler_t));
	if (buffer != NULL) {
		memset(buffer, 0, sizeof(eport_buffer_handler_t));

		/* Create buffer */
		buffer->buffer = xRingbufferCreate(buffer_size, RINGBUF_TYPE_NOSPLIT);
		if (buffer->buffer != NULL) {

			/* Register timeout callback */
			buffer->pkglost_cb = pkglost_cb;
			ret = eport_ok;
		}
	}

	/* Free fail initialization */
	if (ret != eport_ok) {
		if (buffer != NULL) {
			free(buffer);
			buffer = NULL;
		}
	}

	/* Return handler */
	return buffer;
}

eport_handler eport_buffer_wait(eport_buffer_handler buffer, int* evt, void** msg_p, size_t* msg_size, uint32_t wait_block_ms) {
	eport_handler eport_ret = NULL;
	size_t pkg_size;
	if (buffer != NULL) {

		/* Return last packet */
		if (buffer->pkg != NULL) {
			vRingbufferReturnItem(buffer->buffer, buffer->pkg);
			buffer->pkg = NULL;
		}

		/* Block and wait to receive a packet */
		do { buffer->pkg = xRingbufferReceive(buffer->buffer, &pkg_size, EPORT_TICKS_TO_MS(wait_block_ms)); }
		while (buffer->pkg == NULL && wait_block_ms == EPORT_MS_BLOCK);
		if (buffer->pkg != NULL) {

			/* Get sender eport */
			eport_ret = buffer->pkg->eport;

			/* Get event */
			if (evt != NULL) *evt = buffer->pkg->evt;

			/* Read message pointer */
			size_t msg_size_ = pkg_size - EPORT_PKG_SIZE_HEADER;
			if (msg_size != NULL) *msg_size = msg_size_;
			if (msg_p != NULL) *msg_p = msg_size_ > 0 ? buffer->pkg->msg : NULL;

		}

	}

	return eport_ret;
}

////////////////////////////////////
///////////// PRIVATE //////////////
////////////////////////////////////

eport_ret_t eport_buffer_send(eport_buffer_handler buffer, eport_pkg_t* pkg, size_t pkg_size, uint32_t send_block_ms) {
	eport_ret_t ret = eport_err;
	BaseType_t ret_;

	/* Check size */
	if (pkg_size <= xRingbufferGetMaxItemSize(buffer->buffer)) {

		/* Send packet */
		if (xPortInIsrContext()) ret_ = xRingbufferSendFromISR(buffer->buffer, pkg, pkg_size, NULL);
		else {
			do { ret_ = xRingbufferSend(buffer->buffer, pkg, pkg_size, EPORT_TICKS_TO_MS(send_block_ms)); }
			while (ret_ != pdTRUE && send_block_ms == EPORT_MS_BLOCK);
		}
		if (ret_ == pdTRUE) ret = eport_ok;
	}

	/* Package lost callback */
	if (ret && buffer->pkglost_cb != NULL) buffer->pkglost_cb(pkg->eport, pkg->evt);

	return ret;
}
