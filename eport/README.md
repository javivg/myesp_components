# EPORT libray
EPORT (Event - Port) library provides a communication mechanism between tasks for event-driven programming. It generates input and output event interfaces, allowing independence between modules.

There are three basic concepts:

### EPORT
The EPORT ports are the main node of interconnection. The user can send events to an EPORT port that is redirected to all connected EPORTs. An EPORT port may have a BUFFER associated with it to receive such events. An event can have a variable-size message associated with it.

### BUFFER
BUFFERs are elements associated with an EPORT. They receive and store the events and messages sent to be read by the user.

### TIMER
TIMERs are associated with an EPORT to which events and messages can be sent in a timed manner.

## EXAMPLE
The following example shows how **Task 1** is an independent module that exposes an input EPORT and an output EPORT on its interface. Internally it uses a TIMER to generate own timed input events and a BUFFER to receive input events. **Task 2** is a dependent module that includes the first one, connecting to the events it reports thorugh an EPORT and BUFFER. 
![example](img/eport_example.png)
