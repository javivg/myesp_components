/**
 * The MIT License (see LICENSE.txt)
 *
 * @file encoder.h
 *
 * @brief Setup ESP32 PCNT hardware module to receive events from an encoder device.
 *
 * This code has been based on dobairoland from:
 * https://github.com/espressif/esp-idf/tree/5624dff/examples/peripherals/pcnt/rotary_encoder
 *
 * @name Javier Vargas Garcia
 * @date 27/12/2021
 */

#ifndef COMPONENTS_MYESP_COMPONENTS_ENCODER_ENCODER_H_
#define COMPONENTS_MYESP_COMPONENTS_ENCODER_ENCODER_H_

#include "esp_err.h"
#include "driver/pcnt.h"
#include "driver/gpio.h"

/* Forward declarations */
typedef struct encoder_handler_t encoder_handler_t;

/* Handler pointer */
typedef encoder_handler_t* encoder_handler;

/* Events */
typedef enum {
	encoder_evt_right,
	encoder_evt_left,
} encoder_evt_t;

/* Callback function */
typedef void (*encoder_event_cb)(encoder_handler handler, encoder_evt_t evt);

/* Configuration structure */
typedef struct {
	pcnt_unit_t pcnt_unit;
	gpio_num_t gpio_phase_a;
	gpio_num_t gpio_phase_b;
	gpio_pull_mode_t pull;
	uint16_t glitch_filter_us;
	uint32_t sensitivity;
} encoder_config_t;

/* Handler structure */
struct encoder_handler_t {
	encoder_config_t config;
	volatile encoder_event_cb event_cb;
};

esp_err_t encoder_init(encoder_handler* handler, const encoder_config_t* config, encoder_event_cb event_cb, bool install_pcnt_isr);
esp_err_t encoder_evt_start(encoder_handler handler);
esp_err_t encoder_evt_stop(encoder_handler handler);

#endif /* COMPONENTS_MYESP_COMPONENTS_ENCODER_ENCODER_H_ */
