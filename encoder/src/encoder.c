/**
 * The MIT License (see LICENSE.txt)
 *
 * @file encoder.c
 *
 * @brief Encoder source code.
 *
 * This code has been based on dobairoland from:
 * https://github.com/espressif/esp-idf/tree/5624dff/examples/peripherals/pcnt/rotary_encoder
 *
 * @name Javier Vargas Garcia
 * @date 27/12/2021
 */

#include <stdlib.h>
#include <string.h>
#include <sys/cdefs.h>
#include "esp_compiler.h"
#include "esp_log.h"
#include "sys/lock.h"
#include "hal/pcnt_hal.h"

#include "encoder.h"

#define APB_CLK_MHZ 80

// A flag to identify if pcnt isr service has been installed.
static bool is_pcnt_isr_service_installed = false;
// A lock to avoid pcnt isr service being installed twice in multiple threads.
static _lock_t isr_service_install_lock;

static void pcnt_isr_handler(void *arg) {
	encoder_handler handler = arg;
    uint32_t status = 0;
    /* Read interrupt status */
    pcnt_get_event_status(handler->config.pcnt_unit, &status);
    /* Run callback with the event */
    if (status & PCNT_EVT_H_LIM) handler->event_cb(handler, encoder_evt_right);
    else if (status & PCNT_EVT_L_LIM) handler->event_cb(handler, encoder_evt_left);
    /* Reset counter */
    pcnt_counter_clear(handler->config.pcnt_unit);
}

esp_err_t encoder_init(encoder_handler* handler, const encoder_config_t* config, encoder_event_cb event_cb, bool install_pcnt_isr) {
    esp_err_t err = ESP_FAIL;
    encoder_handler new_handler = NULL;

    /* Check inputs */
    if (handler == NULL || config == NULL || event_cb == NULL) goto init_end;

    /* Create a new handler */
    new_handler = malloc(sizeof(encoder_handler_t));
    if (new_handler == NULL) goto init_end;

	/* Reset structure */
	memset(new_handler, 0, sizeof(encoder_handler_t));

	/* Copy configuration */
	memcpy(&new_handler->config, config, sizeof(encoder_config_t));
	new_handler->event_cb = event_cb;

	/* Pull mode */
	err = gpio_reset_pin(new_handler->config.gpio_phase_a);
	err |= gpio_set_pull_mode(new_handler->config.gpio_phase_a, new_handler->config.pull);
	err = gpio_reset_pin(new_handler->config.gpio_phase_b);
	err |= gpio_set_pull_mode(new_handler->config.gpio_phase_b, new_handler->config.pull);
	if (err) goto init_end;

	/* Initialize PCNT channel 0 */
	pcnt_config_t dev_config = {
		.pulse_gpio_num = new_handler->config.gpio_phase_a,
		.ctrl_gpio_num = new_handler->config.gpio_phase_b,
		.channel = PCNT_CHANNEL_0,
		.unit = new_handler->config.pcnt_unit,
		.pos_mode = PCNT_COUNT_DEC,
		.neg_mode = PCNT_COUNT_INC,
		.lctrl_mode = PCNT_MODE_REVERSE,
		.hctrl_mode = PCNT_MODE_KEEP,
		.counter_h_lim = new_handler->config.sensitivity,
		.counter_l_lim = -new_handler->config.sensitivity,
	};
	err = pcnt_unit_config(&dev_config);
	if (err) goto init_end;

	/* Initialize PCNT channel 1 */
	dev_config.pulse_gpio_num = new_handler->config.gpio_phase_b;
	dev_config.ctrl_gpio_num = new_handler->config.gpio_phase_a;
	dev_config.channel = PCNT_CHANNEL_1;
	dev_config.pos_mode = PCNT_COUNT_INC;
	dev_config.neg_mode = PCNT_COUNT_DEC;
	err = pcnt_unit_config(&dev_config);
	if (err) goto init_end;

	/* PCNT pause and reset value */
	pcnt_counter_pause(new_handler->config.pcnt_unit);
	pcnt_counter_clear(new_handler->config.pcnt_unit);

	/* Register interrupt handler in a thread-safe way only once */
	_lock_acquire(&isr_service_install_lock);
	if (!is_pcnt_isr_service_installed && install_pcnt_isr) {
		err = pcnt_isr_service_install(0);
		if (err) goto init_end;
		is_pcnt_isr_service_installed = true;
	}
	_lock_release(&isr_service_install_lock);

	/* Add interrupt handler */
	err = pcnt_isr_handler_add(new_handler->config.pcnt_unit, pcnt_isr_handler, new_handler);
	if (err) goto init_end;

	/* Enable interrupt events */
	pcnt_event_enable(new_handler->config.pcnt_unit, PCNT_EVT_H_LIM);
	pcnt_event_enable(new_handler->config.pcnt_unit, PCNT_EVT_L_LIM);

    /* Configure glitch time */
    err = pcnt_set_filter_value(new_handler->config.pcnt_unit, new_handler->config.glitch_filter_us * APB_CLK_MHZ);
    if (err) goto init_end;
    err = new_handler->config.glitch_filter_us ? pcnt_filter_enable(new_handler->config.pcnt_unit) : pcnt_filter_disable(new_handler->config.pcnt_unit);
    if (err) goto init_end;

init_end:

	/* Free handler if there is an error */
	if (err && new_handler != NULL) {
		free(new_handler);
		new_handler = NULL;
	}

	*handler = new_handler;
    return err;
}

esp_err_t encoder_evt_start(encoder_handler handler) {
    return pcnt_counter_resume(handler->config.pcnt_unit);
}

esp_err_t encoder_evt_stop(encoder_handler handler) {
    return pcnt_counter_pause(handler->config.pcnt_unit);
}
