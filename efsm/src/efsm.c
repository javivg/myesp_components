/**
 * The MIT License (see LICENSE.txt)
 *
 * @file efsm.c
 *
 * @brief EFSM source code.
 *
 * @name Javier Vargas Garcia
 * @date 27/12/2021
 */

#include <stdlib.h>

#include "efsm.h"

efsm_handler efsm_init(const efsm_table_t* efsm_table, size_t efsm_table_size, int init_state) {
	efsm_handler handler = NULL;
	if (efsm_table != NULL && efsm_table_size > 0 && efsm_table_size%sizeof(efsm_table_t) == 0) {
		/* Create handler */
		handler = malloc(sizeof(efsm_handler_t));
		if (handler != NULL) {
			/* Assign table */
			handler->efsm_table = efsm_table;
			handler->efsm_table_entries = efsm_table_size/sizeof(efsm_table_t);
			/* Set init state */
			handler->current_state = init_state;
		}
	}
  	return handler;
}

void efsm_fire(efsm_handler handler, int evt, void* msg) {
	for (int i=0; i<handler->efsm_table_entries; i++) {
		const efsm_table_t* t = &handler->efsm_table[i];
		/* Evaluate current state */
		if (handler->current_state == t->orig_state || t->orig_state == EFSM_ANY_ORIG_STATE) {
			/* Evaluate input event */
			if (t->evt == evt || t->evt == EFSM_ANY_EVT) {
				/* Evaluate guard function */
				efsm_guard_ret_t guard = (t->guard != EFSM_IGNORE_GUARD) ? t->guard(msg) : efsm_guard_true;
				if (guard == efsm_guard_true) {
					/* Action function */
					if (t->action != EFSM_IGNORE_ACTION) t->action(msg);
					/* Change state */
					if (t->dest_state != EFSM_KEEP_DEST_STATE) handler->current_state = t->dest_state;
					break;
				}
			}
		}
	}
}




