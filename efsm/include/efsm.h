/**
 * The MIT License (see LICENSE.txt)
 *
 * @file efsm.h
 *
 * @brief Event-Driven Finite State Machine (EFSM) API.
 *
 * @name Javier Vargas Garcia
 * @date 27/12/2021
 */

#ifndef COMPONENTS_MYESP_COMPONENTS_EFSM_EFSM_H_
#define COMPONENTS_MYESP_COMPONENTS_EFSM_EFSM_H_

#define EFSM_ANY_ORIG_STATE 	-1  	/* Entry valid for any origin state */
#define EFSM_ANY_EVT			-1		/* Entry used for any input event */
#define EFSM_KEEP_DEST_STATE 	-1		/* Destination state not changed */
#define EFSM_IGNORE_GUARD		NULL	/* Ignore guard function and return true */
#define EFSM_IGNORE_ACTION		NULL	/* Ignore action function */

typedef enum {
	efsm_guard_false,
	efsm_guard_true,
} efsm_guard_ret_t;

/* Forward declaration */
typedef struct efsm_handler_t efsm_handler_t;

/* Handler pointer */
typedef efsm_handler_t* efsm_handler;

/* Guard and action functions */
typedef efsm_guard_ret_t (*efsm_guard_f) (void* msg);
typedef void (*efsm_action_f) (void* msg);

/* Transition table */
typedef struct {
  int orig_state;
  int evt;
  efsm_guard_f guard;
  int dest_state;
  efsm_action_f action;
} efsm_table_t;

/* Fsm handler */
struct efsm_handler_t {
  int current_state;
  const efsm_table_t* efsm_table;
  size_t efsm_table_entries;
};

efsm_handler efsm_init(const efsm_table_t* efsm_table, size_t efsm_table_size, int init_state);
void efsm_fire(efsm_handler handler, int evt, void* msg);

#endif /* COMPONENTS_MYESP_COMPONENTS_EFSM_EFSM_H_ */
