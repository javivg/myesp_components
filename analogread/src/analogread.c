/**
 * The MIT License (see LICENSE.txt)
 *
 * @file analogread.c
 *
 * @brief Analogread source code.
 *
 * @name Javier Vargas Garcia
 * @date 18/02/2022
 */

#include "stdio.h"
#include "stdbool.h"
#include "string.h"

#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "sys/lock.h"

#include "analogread.h"

#define ANALOGREAD_DEFAULT_WIDTH 	ADC_WIDTH_BIT_12
#define ANALOGREAD_DEFAULT_ATTEN 	ADC_ATTEN_DB_11
#define ANALOGREAD_DEFAULT_VREF		1100

struct analogread_handler_t {
	adc_unit_t unit;
	adc1_channel_t adc1;
	adc2_channel_t adc2;
	esp_adc_cal_characteristics_t adc_chars;
	int multisampling;
};

static _lock_t isr_configure_adc1_lock;
static bool adc1_configured = 0;

////////////////////////////////////
//////////////// API ///////////////
////////////////////////////////////

esp_err_t analogread_init(analogread_handler* analogread, gpio_num_t gpio, uint32_t multisampling) {
	esp_err_t err = ESP_FAIL;
	analogread_handler new_analogread = NULL;

	/* Check input values */
	if (analogread == NULL && multisampling > 0) goto init_end;

	/* Create handler in heap */
	new_analogread = malloc(sizeof(analogread_handler_t));
	if (new_analogread == NULL) goto init_end;
	memset(new_analogread, 0, sizeof(analogread_handler_t));

	/* Look for channel */
	new_analogread->unit = ADC_UNIT_MAX;
	for (adc1_channel_t adc1 = ADC1_CHANNEL_0; adc1 < ADC1_CHANNEL_MAX; adc1++) {
		gpio_num_t gpio_;
		adc1_pad_get_io_num(adc1, &gpio_);
		if (gpio_ == gpio) {
			new_analogread->unit = ADC_UNIT_1;
			new_analogread->adc1 = adc1;
			break;
		}
	}
	if (new_analogread->unit != ADC_UNIT_MAX) {
		for (adc2_channel_t adc2 = ADC2_CHANNEL_0; adc2 < ADC2_CHANNEL_MAX; adc2++) {
			gpio_num_t gpio_;
			adc2_pad_get_io_num(adc2, &gpio_);
			if (gpio_ == gpio) {
				new_analogread->unit = ADC_UNIT_2;
				new_analogread->adc2 = adc2;
				break;
			}
		}
	}
	if (new_analogread->unit == ADC_UNIT_MAX) {
		err = ESP_ERR_INVALID_ARG;
		goto init_end;
	}

	/* Set ADC 1 width configuration (common for all channels) */
	_lock_acquire(&isr_configure_adc1_lock);
	if (new_analogread->unit == ADC_UNIT_1 && !adc1_configured) {
		adc1_configured = 1;
		err = adc1_config_width(ANALOGREAD_DEFAULT_WIDTH);
	}
	_lock_release(&isr_configure_adc1_lock);
	if (err) goto init_end;

	/* Set attenuation */
	err = new_analogread->unit == ADC_UNIT_1 ? adc1_config_channel_atten(new_analogread->adc1, ANALOGREAD_DEFAULT_ATTEN) : adc2_config_channel_atten(new_analogread->adc2, ANALOGREAD_DEFAULT_ATTEN);
	if (err) goto init_end;

    /* Characterize ADC */
    esp_adc_cal_value_t val_type = esp_adc_cal_characterize(new_analogread->unit, ANALOGREAD_DEFAULT_ATTEN, ANALOGREAD_DEFAULT_WIDTH, ANALOGREAD_DEFAULT_VREF, &new_analogread->adc_chars);
    if (val_type == ESP_ADC_CAL_VAL_NOT_SUPPORTED) err = ESP_FAIL;

    /* Multisampling */
    new_analogread->multisampling = multisampling;

init_end:
	/* Free handler if there is an error */
	if (err && new_analogread != NULL) {
		free(new_analogread);
		new_analogread = NULL;
	}

	*analogread = new_analogread;

	return err;
}

uint32_t analogread_get(analogread_handler analogread) {
	uint32_t voltage = 0;
    uint32_t adc_reading = 0;

    if (analogread != NULL) {

		/* Multisampling */
		for (uint32_t i = 0; i < analogread->multisampling; i++) {
			if (analogread->unit == ADC_UNIT_1) {
				adc_reading += adc1_get_raw(analogread->adc1);
			}
			else {
				int raw;
				adc2_get_raw(analogread->adc2, ANALOGREAD_DEFAULT_WIDTH, &raw);
				adc_reading += raw;
			}
		}
		adc_reading /= analogread->multisampling;

		/* Convert adc_reading to voltage in mV */
		voltage = esp_adc_cal_raw_to_voltage(adc_reading, &analogread->adc_chars);
    }

	return voltage;
}


