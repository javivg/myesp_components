/**
 * The MIT License (see LICENSE.txt)
 *
 * @file analogread.h
 *
 * @brief Read an analog GPIO from ESP32.
 *
 * @name Javier Vargas Garcia
 * @date 18/02/2022
 */

#ifndef COMPONENTS_MYESP_COMPONENTS_ANALOGREAD_INCLUDE_ANALOGREAD_H_
#define COMPONENTS_MYESP_COMPONENTS_ANALOGREAD_INCLUDE_ANALOGREAD_H_

#include "esp_err.h"
#include "hal/gpio_types.h"

/* Structure */
typedef struct analogread_handler_t analogread_handler_t;

/* Handlers */
typedef analogread_handler_t* analogread_handler;

esp_err_t analogread_init(analogread_handler* analogread, gpio_num_t gpio, uint32_t multisampling);

uint32_t analogread_get(analogread_handler analogread);

#endif /* COMPONENTS_MYESP_COMPONENTS_ANALOGREAD_INCLUDE_ANALOGREAD_H_ */
